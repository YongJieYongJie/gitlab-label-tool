# pylint: disable=missing-docstring

import io
import json
import os
import sys

import glt


def test_prompt_for_access_token(capsys):

    # Arrange
    access_token_expected = 'abc'
    single_enter_keypress = '\n'
    sys.stdin = io.StringIO(access_token_expected + single_enter_keypress)

    # Act
    access_token = glt.prompt_for_access_token()

    # Assert
    assert 'Access Token: ' in capsys.readouterr().out
    assert access_token == access_token_expected


def test_prompt_for_project_id(capsys):

    # Arrange
    project_id_expected = '123'
    single_enter_keypress = '\n'
    sys.stdin = io.StringIO(project_id_expected + single_enter_keypress)
    project_id = 13083 # GitLab Community Edition

    # Act
    project_id = glt.prompt_for_project_id()

    # Assert
    assert 'Project ID: ' in capsys.readouterr().out
    assert project_id == project_id_expected


def test_export_labels():

    # Arrange
    project_id = 13083 # GitLab Community Edition
    access_token = os.environ.get('gitlab_access_token', None) \
        or exit('Please set the environment variable `gitlab_access_token`.')
    label_expected_first = json.loads(
        '{"id":8745578,"name":"1st contribution","color":"#428BCA",'
        '"description":"First contribution from a community member",'
        '"text_color":"#FFFFFF","subscribed":false,"priority":null,'
        '"is_project_label":false}')
    label_expected_last = json.loads(
        '{"id":10641658,"name":"writingday::proposed","color":"#428BCA",'
        '"description":"Issues proposed by the GitLab team for participants of '
        'https://www.writethedocs.org/conf/portland/2019/writing-day/ to take '
        'on. ","text_color":"#FFFFFF","subscribed":false,"priority":null,'
        '"is_project_label":true}')

    # Act
    labels_json = glt.export_labels(project_id, access_token)

    # Assert
    assert label_expected_first in labels_json
    assert label_expected_last in labels_json


def test_import_labels():

    # Arrange
    project_id = 14083590 # https://gitlab.com/yongjie-pet/gitlab-label-tool
    access_token = os.environ.get('gitlab_access_token', None) \
        or exit('Please set the environment variable `gitlab_access_token`.')
    labels_for_import = [
        {'name': 'test-label-1', 'color': '#5843AD', 'priority': None},
        {'name': 'test-label-2', 'color': '#cc43AD', 'description': ''},
    ]

    # Act
    glt.import_labels(project_id, access_token, labels_for_import)

    # Assert
    labels = glt.export_labels(project_id, access_token)
    matching_labels = []
    for label in labels_for_import:
        matching_labels += [l for l in labels \
                            if l['name'] == label['name'] \
                            and l['color'] == label['color']]
    assert len(matching_labels) == len(labels_for_import)

    # Tear-down
    glt.delete_labels_by_names(project_id, access_token,
                               ['test-label-1', 'test-label-2'])
