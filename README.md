# GitLab Label Tool

A simple tool to help with exporting and importing labels from GitLab.  
(Essentially a partial wrapper around the official [Labels API](gitlab-labels-api)
.)

# Features

1. Export labels from a particular repository as JSON
2. Copy labels from one repository to another

[gitlab-labels-api]: https://docs.gitlab.com/ee/api/labels.html

# Usage

Run the following command in your favorite shell (with python 3), and follow the
instructions as prompted:

``` shell
$ python ./glt.py
```


# Why

There seems to be some frustration online (and certainly from myself) in
relation to the lack of a easy way to transfer labels from an existing projects
into another project. See relevant issues, discussions etc. at the following:
- https://gitlab.com/gitlab-org/gitlab-ce/issues/39802
- https://gitlab.com/gitlab-org/gitlab-ce/issues/55197
- https://gitlab.com/gitlab-org/gitlab-ce/issues/24547
- https://gitlab.com/gitlab-org/gitlab-ce/issues/24021