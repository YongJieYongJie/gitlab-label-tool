import json
import logging
import urllib.request
from urllib.parse import urlencode

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)


ENDPOINT_GET = 'https://gitlab.com/api/v4/projects/{project_id}/labels' \
    '?page={page}&per_page={per_page}'
ENDPOINT_CREATE = 'https://gitlab.com/api/v4/projects/{project_id}/labels'
ENDPOINT_DELETE = 'https://gitlab.com/api/v4/projects/{project_id}/labels' \
    '?name={name}'


def prompt_for_access_token():
    """Prompts for GitLab access token

    Note: It may not make sense to prompt for the access token without also
    prompting for the Project ID. See `prompt_for_project_id()`.
    """

    return input('Access Token: ')


def prompt_for_project_id():
    """Prompts for the GitLab Project ID
    """

    return input('Project ID: ')


def _get_num_pages(project_id, access_token):

    url = ENDPOINT_GET.format(project_id=project_id, page=1, per_page=99)
    headers = {'PRIVATE-TOKEN': access_token}

    req = urllib.request.Request(url=url, headers=headers, method='GET')
    res = urllib.request.urlopen(req)

    link_header = res.getheader('Link')
    # Example of the header:
    # <https://gitlab.com/api/v4/projects/13083/labels?id=13083&page=2
    # &per_page=99&with_counts=false>; rel="next", <https://gitlab.com/api/v4
    # /projects/13083/labels?id=13083&page=1&per_page=99&with_counts=false>;
    #  rel="first", <https://gitlab.com/api/v4/projects/13083/labels?id=13083
    # &page=9&per_page=99&with_counts=false>; rel="last"

    return int(link_header.split('&page=')[-1][0])


def _get_labels_single_page(project_id, access_token, page=1):

    url = ENDPOINT_GET.format(project_id=project_id, page=page, per_page=99)
    headers = {'PRIVATE-TOKEN': access_token}

    LOGGER.debug('Sending GET request for page %d at URL: %s', page, url)
    req = urllib.request.Request(url=url, headers=headers, method='GET')
    res = urllib.request.urlopen(req)

    res_content = res.read().decode()
    return res_content


def export_labels(project_id, access_token):
    """Retrieves label for `project_id` with `access_token`
    """

    num_pages = _get_num_pages(project_id, access_token)

    labels = []
    for page in range(1, num_pages+1):
        labels += json.loads(
            _get_labels_single_page(project_id, access_token, page))

    return labels


def _sanitize_label(label):

    VALID_PARAMETERS = ['name', 'color', 'description', 'priority'] # pylint: disable=invalid-name

    return {k: v for k, v in label.items() \
            if k in VALID_PARAMETERS and bool(v)}

def _create_label_single(project_id, access_token, label):

    url = ENDPOINT_CREATE.format(project_id=project_id)
    headers = {'PRIVATE-TOKEN': access_token}
    data = urlencode(_sanitize_label(label)).encode('ascii')

    LOGGER.info('Creating label: "%s" in project (ID: %s)',
                label['name'], project_id)
    req = urllib.request.Request(url=url, headers=headers, data=data,
                                 method='POST')
    res = urllib.request.urlopen(req)
    if res.status != 201: # HTTP status code 201 "Created"
        raise Exception('Error creating label: %d %s' % (res.status, res.reason))


def import_labels(project_id, access_token, labels):
    """Create `labels` in project with `project_id` using `access_token`

    For valid attributes of each label, refer to GitLab's official \
    documentation at \
    https://docs.gitlab.com/ee/api/labels.html#create-a-new-label
    """

    for label in labels:
        _create_label_single(project_id, access_token, label)


def delete_labels_by_names(project_id, access_token, names):
    """Delete labels in project with `project_id` by names of the labels
    """

    for name in names:
        url = ENDPOINT_DELETE.format(project_id=project_id, name=name)
        headers = {'PRIVATE-TOKEN': access_token}
        req = urllib.request.Request(url=url, headers=headers, method='DELETE')
        res = urllib.request.urlopen(req)
        if res.status != 204: # HTTP status code 204 "No Content"
            raise Exception('Error connecting: %d %s' % (res.status, res.reason))

def main_export_json():
    """Prompts for Project ID and access token, and exports labels accordingly
    """

    project_id = prompt_for_project_id()
    access_token = prompt_for_access_token()
    labels = export_labels(project_id, access_token)
    print(labels)


def main_transfer_labels():
    """Prompts for Project ID and access token for each of source and
    destination repositories, and transfer the labels from source repository to
    destination repositiory
    """

    print('\n==[Source Repositiory]==')
    src_project_id = prompt_for_project_id()
    src_access_token = prompt_for_access_token()

    print('\n==[Destination Repositiory]==')
    dest_project_id = prompt_for_project_id()
    dest_access_token = prompt_for_access_token()
    if not dest_access_token:
        dest_access_token = src_access_token

    labels = export_labels(src_project_id, src_access_token)
    import_labels(dest_project_id, dest_access_token, labels)



def main():
    """Prompts user for desired action, and delegate to appropriate method(s)
    """

    VALID_OPTIONS = { # pylint: disable=invalid-name
        'EXPORT_JSON': 1,
        'TRANSFER_LABELS': 2,
        }
    DEFAULT_OPTION = VALID_OPTIONS['EXPORT_JSON'] # pylint: disable=invalid-name

    option = input('What would you like to do?\n'
                   '\n'
                   'Options:\n'
                   '[1] Export labels from project to JSON\n'
                   '[2] Transfer labels from one project to another\n'
                   '\n'
                   'Choice (Default: 1):\n')

    try:
        option = int(option)
    except ValueError:
        option = DEFAULT_OPTION

    if option not in VALID_OPTIONS.values():
        option = DEFAULT_OPTION

    if option == VALID_OPTIONS['EXPORT_JSON']:
        main_export_json()

    if option == VALID_OPTIONS['TRANSFER_LABELS']:
        main_transfer_labels()


if __name__ == '__main__':

    main()
